package com.lazerycode.selenium;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 3/31/14
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommonUtil {
    public void pause(int secs) {
        try {
            Thread.sleep(secs * 1000);
        } catch (InterruptedException interruptedException) {

        }
    }

    public String currentDateAndTime() {
        int day, month, year;
        int second, minute, hour;
        GregorianCalendar date = new GregorianCalendar();

        day = date.get(Calendar.DAY_OF_MONTH);
        month = date.get(Calendar.MONTH);
        year = date.get(Calendar.YEAR);

        second = date.get(Calendar.SECOND);
        minute = date.get(Calendar.MINUTE);
        hour = date.get(Calendar.HOUR);

        String currentDateAndTime = String.valueOf(year) + String.valueOf(month) + String.valueOf(day)
                                    + String.valueOf(hour) + String.valueOf(minute) + String.valueOf(second);
        return currentDateAndTime;
    }

    public String calcDateBasedOnToday(int x){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, x);
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        return df.format(cal.getTime());
    }

    /**
     * Select Dropdown
     *
     * @param dropdwon
     */
    public void dropdownmenu(String dropdwon,String elementLocator) {
//
//        Select dropdown = new Select(driver.findElement(By.id(elementLocator)));
//        dropdown.selectByValue(dropdwon);
    }



}
