package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/30/14
 * Time: 2:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class DispositionPage extends Page{

    @FindBy(id="h2")
    public WebElement pageTitle;

    private WebDriver driver;

    public DispositionPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void armDisposition (WebDriver driver) {
        CommonUtil cU = new CommonUtil();
        if (!(pageTitle.getText().contains("Disposition"))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Disposition Page.");
        }
    }
}
