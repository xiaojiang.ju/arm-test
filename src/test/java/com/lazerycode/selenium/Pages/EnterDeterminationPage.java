package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.UploadFile;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/24/14
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class EnterDeterminationPage extends Page{
    @FindBy(id="acceptForId")
    public WebElement acceptForId;

    public WebElement acknowledgeReceipt;

    @FindBy(id="identificationPossible")
    public WebElement identificationPossible;

    @FindBy(id="identificationNotPossible")
    public WebElement identificationNotPossible;

    @FindBy(id="taxonomicName")
    public WebElement taxonomicName;

    @FindBy(id="method")
    public WebElement method;

    @FindBy(id="determinationType")
    public WebElement finalID;

    @FindBy(id="uploadPestImage")
    public WebElement uploadPestImage;

    @FindBy(id="uploadPest")
    public WebElement uploadPest;

    @FindBy(id="save")
    public WebElement saveDetermination;

    @FindBy(id="submit")
    public WebElement submitDetermination;

    @FindBy(id="next")
    public WebElement quarantineRecommendation;

    @FindBy(className="blueSubmitButton")
    public WebElement printShipmentInfo;

    @FindBy(xpath=".//*[@id='flowModel']/div/table/tbody/tr[25]/td[2]/img")
    public WebElement pest_Discipline;

    @FindBy(xpath=".//*[@id='flowModel']/div/table/tbody/tr[25]/td[2]/img")
    public WebElement quarantineStatus;

    @FindBy(id="determinationremarks")
    public WebElement determinationremarks;

    @FindBy(id="h2")
    public WebElement pageTitle;

    private WebDriver driver;

    public EnterDeterminationPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void armEnterDetermination (WebDriver driver) {
        CommonUtil cU = new CommonUtil();
        if (!(pageTitle.getText().contains("Enter Determination"))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Enter Determination Page.");
        }
        acceptForId.click();
        acknowledgeReceipt.isSelected();
        identificationPossible.isSelected();
        identificationNotPossible.isDisplayed();
        taxonomicName.sendKeys("Tortricidae");
        determinationremarks.isDisplayed();
        method.sendKeys("Culture");     //Barcode (Molecular)  //Digital Imagery  //ELISA
        finalID.sendKeys("Yes");
        //uploadPestImage.click();
        //TODO: add image here
        //uploadPest.click();
        saveDetermination.click();
        //printShipmentInfo.click();
        //submitDetermination.click();
        quarantineRecommendation.click();


    }
}
