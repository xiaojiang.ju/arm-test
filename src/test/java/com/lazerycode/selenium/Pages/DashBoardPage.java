package com.lazerycode.selenium.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/30/14
 * Time: 2:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class DashBoardPage {

//    String DRNumber = "";
//    @FindBy(linkText = DRNumber)
//    public WebElement DRNumberText;

    @FindBy(id = "#pg1DataTbl")
    public WebElement AI_DR_Table;

    @FindBy(id = "pg3DataTbl")
    public WebElement EAN_Table;

    @FindBy(id = "pg3ZoomInOutId")
    public WebElement EAN_ZoomInOut_Button;


    @FindBy(id = "pg2DataTbl")
    public WebElement Inspector_DR_Table;

//    @FindBy(xpath = "//div[@id='USDAheader']/table/tbody/tr[1]/td[@class='row2hdrbck'][1]/div/a")
//    public WebElement logout;

//    @FindBy(linkText="Logout")
//    public WebElement logout;

    public DashBoardPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void select_AI_DR_NumLink(WebDriver driver, String DRNum){
        if (!(AI_DR_Table.getText().contains(DRNum))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("Can't find DRNum in the table.");
        }
        driver.findElement(By.linkText(DRNum)).click();
    }

    public void select_Inspector_EAN_ID_Link(WebDriver driver, String LRN){
        if (!(Inspector_DR_Table.getText().contains(LRN))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("Can't find LRN in the table.");
        }
        EAN_ZoomInOut_Button.click();
        driver.findElement(By.xpath("//td[contains(text(),LRN)]/preceding::link(text(),'Create')")).click();
    }

    public void select_Inspector_EAN_ID_Link_by_MasterAirBill(WebDriver driver, String MasterAirWayBillNum){
        if (!(Inspector_DR_Table.getText().contains(MasterAirWayBillNum))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("Can't find MasterAirWayBillNum in EAN table.");
        }
        EAN_ZoomInOut_Button.click();
        driver.findElement(By.xpath("//td[contains(text(),getMasterAirWayBillNum)]/preceding::link(text(),'Create')")).click();
    }
}
