package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
* Created with IntelliJ IDEA.
* User: alexju
* Date: 4/10/14
* Time: 2:05 PM
* To change this template use File | Settings | File Templates.
*/
public class InspectionPage extends Page{

    @FindBy(id="commodities0.commodityInspectionDate")
    public WebElement commodityInspectionDate;

    @FindBy(id="commodities0.quantityInspected")
    public WebElement quantityInspected;

    @FindBy(id="commodities0.plantConditionId")
    public WebElement plantConditionId;

    @FindBy(id="commodities0.remark")
    public WebElement inspectionRemark;

    @FindBy(id="commodities0.pestPresentFlag")
    public WebElement pestPresentFlag;

    @FindBy(xpath="//input[@id='save']")
    public WebElement saveInspection;

    @FindBy(xpath="//input[@id='pest']")
    public WebElement addPest;

    @FindBy(id="EAN")
    public WebElement addNonPestEAN;

    @FindBy(id="disposition")
    public WebElement addDisposition;

    private WebDriver driver;

    public InspectionPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void armInspection (WebDriver driver) {

        // Check that we're on the right page.
        if (!"Enter Inspection Details".equals(pageTitle.getText())) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Inspection Page.");
        }

        //Enter Inspection Information screen
        CommonUtil cU = new CommonUtil();
        String today = cU.calcDateBasedOnToday(0);
        //Today is already populated.
        //commodityInspectionDate.sendKeys(today);

        quantityInspected.sendKeys("3");
        inspectionRemark.sendKeys("commodity Remark");

        //plantConditionId.sendKeys("Good");
        Select dropDownplantConditionId = new Select(plantConditionId);
        dropDownplantConditionId.selectByVisibleText("Good");

        //pestPresentFlag.sendKeys("Yes");
        Select dropDownPestFound = new Select(pestPresentFlag);
        dropDownPestFound.selectByVisibleText("Yes");
//        saveInspection.click();
        addPest.click();
    }


 }
