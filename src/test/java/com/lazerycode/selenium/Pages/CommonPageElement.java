package com.lazerycode.selenium.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/29/14
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommonPageElement {

    @FindBy(xpath=".//*[@id='USDAheader']/table/tbody/tr[1]/td[2]/div/a")
    public WebElement logout;
//
//    @FindBy(linkText="Logout")
//    public WebElement logout;

    public CommonPageElement(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void armLogout(WebDriver driver){
        if(logout.isDisplayed())
        logout.click();
    }
}
