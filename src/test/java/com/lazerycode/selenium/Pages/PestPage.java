package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/18/14
 * Time: 12:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class PestPage extends Page {

    @FindBy(id="RadioGroup1_2")
    public WebElement hostSelectCommodity;

    //Select other material
    @FindBy(id="RadioGroup1_3")
    public WebElement hostOtherMaterial;

    //Select other material
    @FindBy(id="railPastDiagnostic.select.material")
    public WebElement pestHomeMaterial;

    @FindBy(id="diagnostic.radio.pest")
    public WebElement selectPestFound;

    @FindBy(id="checkboxA0")
    public WebElement checkboxA0_PestTable;

    @FindBy(id="railPastDiagnostic.checkbox.unsurecountry0")
    public WebElement unsureCountry0;

    @FindBy(id="railPastDiagnostic.select.hostProximit0")
    public WebElement hostProximit0;

    @FindBy(id="railPastDiagnostic.select.hostPart0")
    public WebElement hostPart0;

    @FindBy(id="railPastDiagnostic.select.cargoStatus0")
    public WebElement cargoStatus0;

    @FindBy(id="addPestCommodityModels0.releaseJustification")
    public WebElement releaseJustification;

    //3. Select a Pest/Pest Discipline

    @FindBy(id="diagnostic.radio.pest")
    public WebElement pestHostOnMarterial;

    @FindBy(id="taxonomicName")
    public WebElement pestNameField;

    @FindBy(id="date.ppqDetermination.datedetermined")
    public WebElement dateDetermined;

    @FindBy(id="diagnostic.radio.pestdiscipline")
    public WebElement pestDiscipline;

    @FindBy(xpath="//img[@id='button.ppqDetermination.search']")
    public WebElement pestSearchButton;

    @FindBy(xpath="//select[@id='selectKingdomId']")
    public WebElement selectKingdomId;

    @FindBy(xpath=".//*[@id='_body']/div/form/table/tbody/tr[10]/td[8]/input")
    public WebElement pestSearchOnPopup;

    @FindBy(xpath=".//*[@id='_body']/div/div[1]/table/tbody/tr[1]/td[1]/a")
    public WebElement chooseFirstPestOnSearch;

    //selectKingdomId.sendKeys("Animal");
    //pestSearchOnPopup.click();
    //chooseAbronia.click();

    @FindBy(xpath="//select[@id='select.ppqDetermination.method']")
    public WebElement pestFoundMethod;

    @FindBy(id="add")
    public WebElement addPest;

    //add Pest detail information
    @FindBy(id="text.ppqDetermination.aliveimmature")
    public WebElement aliveimmaturePestNumber;

    @FindBy(id="text.ppqDetermination.deadimmature")
    public WebElement deadimmaturePestNumber;

    @FindBy(id="textarea.ppqDetermination.determinationremarks")
    public WebElement determinationRemarks;

    @FindBy(id="addPestModels0.picked1")
    public WebElement addPestModels0;

    @FindBy(id="addPestModels0.diagnosticRequestNumber")
    public WebElement diagnosticRequestNumber;

//    @FindBy(xpath="//input[@type='submit']")
    @FindBy(xpath=".//*[@id='Buttonset2']/div/input[3]")
    public WebElement submitInspection;

    @FindBy(xpath=".//*[@id='overlay']/form/div/table/tbody/tr[5]/td[2]/input")
    public WebElement eventID;

    public PestPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String drNumber;
    private WebDriver driver;

    public void armPest(WebDriver driver) {

        // Check that we're on the right page.
        if (!"Add Pest".equals(pageTitle.getText())) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Inspection Page.");
        }

        CommonUtil cU = new CommonUtil();
        String today = cU.calcDateBasedOnToday(0);

        //Enter Inspection Information screen
        //Select other material
        hostSelectCommodity.isSelected();
        hostSelectCommodity.click();
        // Either select hostSelectCommodity or hostOtherMaterial Option
        //hostOtherMaterial.click();
        //pestHostOnMarterial.sendKeys("Wood Packing Material");
        //pestHomeMaterial.sendKeys("Wood Packing Material");

        //Optional
        checkboxA0_PestTable.click();
        selectPestFound.sendKeys("Wood Packing Material");
        unsureCountry0.click();
        hostProximit0.sendKeys("In");
        hostPart0.sendKeys("Bark");
        cargoStatus0.sendKeys("Held for this pest");  // Held for other reason   Released
        releaseJustification.sendKeys("releaseJustification");

        //pest discipline vs pest name
        //pestDiscipline.sendKeys("Entomology");

        //Store the current window handle
        String winHandleBefore = driver.getWindowHandle();
        //Perform the click operation that opens new window
        pestSearchButton.click();
        //Switch to new window opened
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }

        // Perform the actions on new window
        selectKingdomId.sendKeys("Animal");
        pestSearchOnPopup.click();
        chooseFirstPestOnSearch.click();

        //Close the new window, if that window no more required
        //driver.close();

        //Switch back to original browser (first window)
        driver.switchTo().window(winHandleBefore);

        // Instead of search pest, type pest name into the dropdown list.
        //((JavascriptExecutor)driver).executeScript("arguments[0].value=arguments[1]", pestNameField, "Tortricidae");

        dateDetermined.sendKeys(today);
        pestFoundMethod.sendKeys("Culture");

        //select discipline
        aliveimmaturePestNumber.click();
        aliveimmaturePestNumber.sendKeys("9");
        deadimmaturePestNumber.click();
        deadimmaturePestNumber.sendKeys("2");
        determinationRemarks.sendKeys("Determination Remarks");
        addPest.click();

        addPestModels0.click();
        String DRNumber = diagnosticRequestNumber.getAttribute("value");
        drNumber =  DRNumber;
        System.out.println("\nDR # = " + DRNumber);
        cU.pause(5);
        submitInspection.click();

        //handle pop-ups
        winHandleBefore = driver.getWindowHandle();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
            System.out.println(driver.getTitle());
        }
        driver.switchTo().window(winHandleBefore);

    }

    public String armGetDRNum(){
        System.out.println("\nDR # = " + drNumber);

        return drNumber;
    }
}
