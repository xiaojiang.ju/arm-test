package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static junit.framework.Assert.assertEquals;


/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/29/14
 * Time: 11:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class EANPage extends Page{


    @FindBy(id="shipperId")
    public WebElement shipperId;

    @FindBy(id="cconsignerId")
    public WebElement consignerId;

    @FindBy(id="sameAsConsigneeFlag")
    public WebElement sameAsConsigneeFlag;

    @FindBy(id="address1")
    public WebElement address1;

    @FindBy(id="addressName")
    public WebElement addressName;

    @FindBy(id="addressCity")
    public WebElement addressCity;

    @FindBy(id="stateMap")
    public WebElement stateMap;

    @FindBy(id="addressZip")
    public WebElement addressZip;

    @FindBy(id="eanStatusMap")
    public WebElement eanStatusMapDropDown;

    @FindBy(id="cfrRegulationsCheckBox")
    public WebElement cfrRegulationsCheckBox_All;

    @FindBy(css="img.ui-datepicker-trigger")
    public WebElement datepickerTrigger;

    @FindBy(id="issuedDate")
    public WebElement issuedDate;

    @FindBy(id="responseTime")
    public WebElement responseTime;

    @FindBy(id="timeUnitMap")
    public WebElement timeUnitMap;         //Hours, Days

    @FindBy(css="img.ui-datepicker-trigger")
    public WebElement datepickerTriggerEAN;

    @FindBy(id="localOfficerContactPhoneNumber")
    public WebElement localOfficerContactPhoneNumber;

    private WebDriver driver;

    public EANPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void armEAN(WebDriver driver) {

        // Check that we're on the right page.
        if (!" EAN Information".equals(pageTitle.getText())) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the  EAN Information Page.");
        }

        sameAsConsigneeFlag.click();
        assertEquals(true, sameAsConsigneeFlag.isSelected());
        if (sameAsConsigneeFlag.isSelected()) {
            sameAsConsigneeFlag.click();
            assertEquals(false, sameAsConsigneeFlag.isSelected());
        }
        address1.sendKeys("Address on EAN page");
        addressName.sendKeys("HERBTHYME FARMS");
        addressCity.sendKeys("Alexandria");
        stateMap.sendKeys("ZAC Zacatecas");
        addressZip.sendKeys("22180");
        eanStatusMapDropDown.sendKeys("Required-Present");    //Required-Missing Certifications,    Required-Missing Addendums, Not Required

        CommonUtil cU = new CommonUtil();
        String today = cU.calcDateBasedOnToday(0);
        //Today is already populated.

        cfrRegulationsCheckBox_All.click();
        issuedDate.sendKeys(today);
        responseTime.sendKeys("5");
        timeUnitMap.sendKeys("Hours");
        localOfficerContactPhoneNumber.sendKeys("703-555-5555");
    }
}
