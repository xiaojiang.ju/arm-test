package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
* Created with IntelliJ IDEA.
* User: alexju
* Date: 4/10/14
* Time: 1:55 PM
* To change this template use File | Settings | File Templates.
*/
public class CommodityPage extends Page{

    @FindBy(id="permitTypeId")
    public WebElement permitTypeId;      //PPQ 621

    @FindBy(id="permitNumber")
    public WebElement permitNumber;

    @FindBy(id="userCommodityId")
    public WebElement userCommodityId;

    @FindBy(id="producerId")
    public WebElement producerId;

    @FindBy(id="propagativeMaterial")
    public WebElement propagativeMaterial;

    @FindBy(id="countryOfOrgin")
    public WebElement countryOfOrgin;

    @FindBy(id="quantity")
    public WebElement quantity;
    @FindBy(id="units")
    public WebElement units;
    @FindBy(id="woodPackingMaterial")
    public WebElement woodPackingMaterial;

    @FindBy(id="add")
    public WebElement addCommodity;

    @FindBy(id="next")
    public WebElement nextOnCommodity;

    @FindBy(id="commodities0.commodityInspectionDate")
    public WebElement commodities0_commodityInspectionDate;
    @FindBy(id="commodities0.quantityInspected")

    public WebElement commodities0_quantityInspected;
    @FindBy(id="commodities0.plantConditionId")
    public WebElement commodities0_plantConditionId;

    @FindBy(id="commodities0.remark")
    public WebElement commodities0_remark;

    @FindBy(id="commodities0.pestPresentFlag")
    public WebElement commodities0_pestPresentFlag;

    @FindBy(xpath="//input[@id='save']")
    public WebElement saveCommodity;

    @FindBy(xpath="//img[@alt='Search for Shipper']")
    public WebElement search_for_shipper;

    @FindBy(id="searchTaxonomy")
    public WebElement searchCommodity;

    @FindBy(xpath=".//*[@id='_body']/div/form/table/tbody/tr[10]/td[8]/input")
    public WebElement searchCommodityPlantOnPopUp;

    @FindBy(xpath=".//*[@id='_body']/div/div[1]/table/tbody/tr[1]/td[1]/a")
    public WebElement firstCommodityOnPopUp;


//    @FindBy(xpath="//input[@classname='dashboardSubmit' and type='submit']")
//    public WebElement producerSearchbutton;
//    @FindBy(xpath="//input[@id='Buttonset']/div/following-sibling::input")
//    public WebElement producerSearchbutton;
    @FindBy(xpath=".//*[@id='Buttonset']/div/input[2]")
    public WebElement producerSearchbutton;


//    @FindBy(xpath="input[@value='Search']")
//    public WebElement producerSearchbutton;

    @FindBy(xpath=".//*[@id='shipperTable']/div/table/tbody/tr[1]/td[1]/a")
    public WebElement chooseProducerFirst;

    @FindBy(id="producerId")
    public WebElement producerDropdown;

    private WebDriver driver;
    public CommodityPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    public void armCommodity (WebDriver driver) {
        CommonUtil cU = new CommonUtil();
        // Check that we're on the right page.
        if (!"Enter Commodity Information".equals(pageTitle.getText())) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Commodity Page.");
        }
        else {
            //Enter Shipment information screen
            permitTypeId.sendKeys("PPQ 621");
            String permitNum = String.valueOf(System.currentTimeMillis());
            permitNumber.sendKeys(permitNum);

            //userCommodityId.sendKeys("Abelia");
            String winHandleBefore = driver.getWindowHandle();
            searchCommodity.click();
            for(String winHandle : driver.getWindowHandles()){
                driver.switchTo().window(winHandle);
            }
            searchCommodityPlantOnPopUp.click();
            firstCommodityOnPopUp.click();
            driver.switchTo().window(winHandleBefore);

            //Get the first producer/grower name from search popup
            winHandleBefore = driver.getWindowHandle();
            //Perform the click operation that opens new window
            search_for_shipper.click();
            //Switch to new window opened
            for(String winHandle : driver.getWindowHandles()){
                driver.switchTo().window(winHandle);
            }

            // Perform the actions on new window
            producerSearchbutton.click();
            chooseProducerFirst.click();
            // Close the new window, if that window no more required
            // driver.close();
            //Switch back to original browser (first window)
            driver.switchTo().window(winHandleBefore);
            //continue with original browser (first window)

            //1 Can't send key directly
            //producerDropdown.sendKeys("Test Farm 1, 45 Grove St, Grovetown, VER 34567");
            //2 Javascript
            //((JavascriptExecutor)driver).executeScript("arguments[0].value=arguments[1]", producerDropdown, "Test Farm 1, 45 Grove St, Grovetown, VER 34567");

            propagativeMaterial.sendKeys("CITES");
            propagativeMaterial.sendKeys("Bulbs/Corms/Rhizomes");
            countryOfOrgin.sendKeys("Aruba");
            quantity.sendKeys("100");
            units.sendKeys("Square Meters");
            woodPackingMaterial.sendKeys("Compliant");
            addCommodity.click();
            nextOnCommodity.click();
        }
    }
}




