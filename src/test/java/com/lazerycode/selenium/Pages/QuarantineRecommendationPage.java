package com.lazerycode.selenium.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/30/14
 * Time: 2:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class QuarantineRecommendationPage extends Page{
    @FindBy(id="quarantineRec")
    public WebElement quarantineRec;

    @FindBy(id="previous")
    public WebElement Back;
    @FindBy(id="cancel")
    public WebElement Return;
    @FindBy(id="next")
    public WebElement TreatmentRec;
    @FindBy(id="AIfinish")
    public WebElement Submit;
    @FindBy(id="h2")
    public WebElement pageTitle;

    public QuarantineRecommendationPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    public void armNoPhytoAction(WebDriver driver){ //Only call this when you are already on the logon page
        if (!(pageTitle.getText().contains("Enter Quarantine Recommendation"))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the  Enter Quarantine Recommendation Page.");
        }
        //Scenario 2
        Select dropDown = new Select(quarantineRec);
        dropDown.selectByVisibleText("No Phytosanitary Action");
        Submit.click();

        //Scenario 5
        //Select dropDown = new Select(quarantineRec);
        //dropDown.selectByVisibleText("Phytosanitary Action");
        //Submit.click();


    }
}
