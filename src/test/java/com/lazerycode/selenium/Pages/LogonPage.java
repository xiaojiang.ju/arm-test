package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/9/14
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class LogonPage extends Page{

//      @FindBy(xpath="//select[@id='usernameLogInDropDown']")
//        @FindBy(xpath=".//*[@id='overlay']/form/div/table/tbody/tr[4]/td[2]/input")
//        @FindBy(xpath="//select[@id='usernameLogInDropDown']")
        @FindBy(xpath=".//*[@id='username']/input")
        public WebElement userNameField;

        @FindBy(xpath = ".//*[@id='password']/input")
        public WebElement passwordField;

        @FindBy(xpath=".//*[@id='loginButton']")
        public WebElement loginButton;

        @FindBy(xpath="//*[@id='USDAheader']/table/tbody/tr[1]/td[1]/img")
        public WebElement USDAImg;

        @FindBy(id="_welcomeBody")
        public WebElement pageTitle;

        public LogonPage(WebDriver driver){
            PageFactory.initElements(driver, this);
        }

        private WebDriver driver;


        public void armLogin(WebDriver driver, String username, String password){ //Only call this when you are already on the logon page
        if (!(driver.getTitle().contains("ARM - Arm Homepage"))) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Logon Page.");
        }

//        loginButtonOnPage.click();
//        Select dropDown= new Select(userNameField.findElement(By.xpath("//select[@id='usernameLogInDropDown']")));
//        dropDown.selectByVisibleText(username);

//        Select dropDown= new Select(userNameField.findElement(By.xpath("//select[@id='usernameLogInDropDown']")));
//        dropDown.selectByVisibleText(username);
        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
        loginButton.click();

        //Waiting should also be done after the function
    }
}
