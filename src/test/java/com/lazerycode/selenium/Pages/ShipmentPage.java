package com.lazerycode.selenium.Pages;

import com.lazerycode.selenium.CommonUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static junit.framework.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 4/8/14
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class ShipmentPage extends Page{

    @FindBy(id="shipment_dashbaord_type")
    public WebElement dashboardShipmentType;

    @FindBy(id="shipmentDashboard_new")
    public WebElement dashboardShipmentNewButton;

    @FindBy(id="portOfArrival")
    public WebElement portOfArrival;

    @FindBy(id="countryOfLading")
    public WebElement countryOfLading;

    @FindBy(className="glyphicon-calendar")
    public WebElement datepickerTrigger;

    @FindBy(className="ui-datepicker-today")
    public WebElement datepickerToday;

    @FindBy(id="arrivedInUsDatepicker")
    public WebElement arrivedInUsDatepicker;

    @FindBy(id="inStationDateTimeDatepicker")
    public WebElement inStationDateTimeDatepicker;

    @FindBy(id="localReferenceNumber")
    public WebElement localReferenceNumber;

    @FindBy(id="shipmentTypeNumberModelForExisting0.number")
    public WebElement masterAirWayBill0;

    @FindBy(id="shipmentTypeNumberModelForExisting1.number")
    public WebElement shipmentTypeNumberModelForExisting1;

    @FindBy(id="shipmentTypeNumberModelForExisting2.number")
    public WebElement shipmentTypeNumberModelForExisting2;

    @FindBy(id="shipmentTypeNumberModelForExisting3.number")
    public WebElement shipmentTypeNumberModelForExisting3;

    @FindBy(id="shipmentTypeNumberModelForExisting4.number")
    public WebElement shipmentTypeNumberModelForExisting4;

    @FindBy(id="shipmentTypeNumberModelForExisting5.number")
    public WebElement shipmentTypeNumberModelForExisting5;

    @FindBy(id="addShipmentType")
    public WebElement addShipmentType;

    @FindBy(id="addShipmentNumber")
    public WebElement addShipmentNumber;

    @FindBy(id="delete")
    public WebElement deleteRow;

    @FindBy(id="add")
    public WebElement addShipment;

    @FindBy(id="shipperId")
    public WebElement shipperId;

    @FindBy(id="brokerId")
    public WebElement brokerId;

    @FindBy(id="consignerId")
    public WebElement consignerId;

    @FindBy(xpath="//select[@id='brokerId']/following-sibling::img")
    public WebElement searchBrokerButton;

    @FindBy(xpath=".//*[@id='Buttonset']/div/input[2]")
    public WebElement searchBrokerButtonOnPopUp;

    @FindBy(xpath=".//*[@id='shipperTable']/div/table/tbody/tr[1]/td[1]/a")
    public WebElement chooseFirstBroke;

    @FindBy(id="next")
    public WebElement nextButtonShipment;

    @FindBy(id="airportMap")
    public WebElement airportDropdown;

    @FindBy(id="airlineMap")
    public WebElement airlineDropDown;

    @FindBy(id="flightNumberId")
    public WebElement flightNumberId;

//    @FindBy(id="h1")
//    public WebElement pageTitle;

    private WebDriver driver;
    public String LRN, masterAirWayBillNum;

    public ShipmentPage (WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void armShipment(WebDriver driver, String Pathway) {
        // Check that we're on the right page.
        CommonUtil cU = new CommonUtil();
        String pathwaySelection = Pathway;
        dashboardShipmentType.sendKeys("Mail");
        dashboardShipmentType.sendKeys(pathwaySelection);
        dashboardShipmentNewButton.click();
        if (!driver.getTitle().contains("Shipment Information")) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the Shipment Page.");
        }
        else {
            //Enter Shipment information screen
            portOfArrival.sendKeys("Anchorage");
            airportDropdown.sendKeys("ACA Acapulco [General Juan N. Alvarez]");
            airlineDropDown.sendKeys("ACC Accra [Kotoka]");
            flightNumberId.sendKeys("Alex123");
            countryOfLading.sendKeys("Aruba");
            datepickerTrigger.click();
//            String today = datepickerToday.getText();
            String arriveDate = arrivedInUsDatepicker.getAttribute("value");
            String pisDate = inStationDateTimeDatepicker.getAttribute("value");

            SimpleDateFormat sm = new SimpleDateFormat("MM/dd/yyyy");

            Date today_SDF = new Date();

            String strToday = sm.format(today_SDF);
            System.out.println(strToday);

            String strYesterday = cU.calcDateBasedOnToday(-1);
            System.out.println(strYesterday);

            assertEquals(strYesterday, arriveDate);
            assertEquals(strToday, pisDate);

//            System.out.print(today);
            System.out.print(" = today from datepicker" + "\narrive date is = ");
            System.out.print(arriveDate);

            String masterAirWayBill = String.valueOf(System.currentTimeMillis());
            LRN="LRN"+masterAirWayBill;
            localReferenceNumber.sendKeys(LRN);
            masterAirWayBillNum =  masterAirWayBill;
            masterAirWayBill0.sendKeys(masterAirWayBill);
            //masterAirWayBill0.sendKeys("0");
            shipmentTypeNumberModelForExisting1.sendKeys(masterAirWayBill+ "001");
            shipmentTypeNumberModelForExisting2.sendKeys(masterAirWayBill+ "002");
            shipmentTypeNumberModelForExisting3.sendKeys(masterAirWayBill+ "003");
            shipmentTypeNumberModelForExisting4.sendKeys(masterAirWayBill+ "004");
            shipmentTypeNumberModelForExisting5.sendKeys(masterAirWayBill+ "005");
            addShipmentType.sendKeys("Master Air Waybill");
            addShipmentNumber.sendKeys(masterAirWayBill+"006");
            shipperId.sendKeys("Global Transport, Inc");
            //brokerId.sendKeys("Test Broker 1");
            String winHandleBefore = driver.getWindowHandle();
            searchBrokerButton.click();
            //Switch to new window opened
            for(String winHandle : driver.getWindowHandles()){
                driver.switchTo().window(winHandle);
            }
            // Perform the actions on new window
            searchBrokerButtonOnPopUp.click();
            chooseFirstBroke.click();
            // Close the new window, if that window no more required
            // driver.close();
            //Switch back to original browser (first window)
            driver.switchTo().window(winHandleBefore);

            consignerId.sendKeys("CONTEL FRESH INC");
            addShipment.click();
            nextButtonShipment.click();
        }
    }

    public String getLRN(){
        return LRN;
    }

    public String getMasterAirWayBillNum(){
        return masterAirWayBillNum;
    }
}
