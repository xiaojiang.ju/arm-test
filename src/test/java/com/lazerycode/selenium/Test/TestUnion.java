//import org.apache.commons.collections.ListUtils;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Set;
//import java.util.TreeSet;
//import org.junit.*;
//  /**
// * Created with IntelliJ IDEA.
// * User: alexju
// * Date: 6/24/14
// * Time: 12:34 PM
// * To change this template use File | Settings | File Templates.
// */
//
//
//public class TestUnion{
//
//// ----------------------------------------------------------------------------------
//String[] a = new String[] { "1", "3", "5", "6", "7", "8" };
//String[] b = new String[] { "2", "3", "5", "7", "9" };
//
//// using apache commons-collections 1.0+
//
//List intersection = ListUtils.intersection(Arrays.asList(a),
//        Arrays.asList(b));
//assert Arrays.equals(intersection.toArray(), new String[] { "3", "5", "7" });
//
//List union = ListUtils.union(Arrays.asList(a), Arrays.asList(b));
//Set unionUnique = new TreeSet(union);
//assert Arrays.equals(unionUnique.toArray(),
//        new String[] { "1", "2", "3", "5", "6", "7", "8", "9" });
//
//List diff = ListUtils.subtract(Arrays.asList(a), Arrays.asList(b));
//assert Arrays.equals(diff.toArray(), new String[] { "1", "6", "8" });
//
//}