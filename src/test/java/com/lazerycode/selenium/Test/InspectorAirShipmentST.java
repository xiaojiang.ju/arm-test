package com.lazerycode.selenium.Test;

import com.lazerycode.selenium.ScreenshotListener;
import com.lazerycode.selenium.SeleniumBase;
import com.lazerycode.selenium.CommonUtil;
//import org.junit.Test;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 3/25/14
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */

@Listeners(ScreenshotListener.class)
public class InspectorAirShipmentST extends SeleniumBase {

    // customized wait function
//    public static boolean wait_pageLoad(long seconds){
//        WebDriverWait wait = new WebDriverWait.FIVE_HUNDRED_MILLIS();
//        WebDriver driver = getDriver();
//        return wait.until(driver.findElement(By.xpath("//*[not (.='')]")));
//    }
    public static ResourceBundle _prop = ResourceBundle.getBundle("dev");

    @Test
    public void testLoginAI() throws Exception {
        CommonUtil cU = new CommonUtil();
        //QA environment
//        url = new url_string(_prop.getString("url_string"));

        String url_string = _prop.getString("url_string");
        System.out.println(url_string);
        if (url_string == null){
            System.err.println("Unknown browser specified, defaulting to Dev URL http://10.101.25.23:8080/armWeb2/ ");
            url_string ="http://10.101.25.23:8090/armWeb2/";
        }

        WebDriver driver = getDriver();
        driver.get(url_string);
//      driver.get("http://10.101.25.23:8080/armWeb2/");

        driver.findElement(By.id("loginBtn")).click();
        assertEquals("Arm Homepage", driver.getTitle());

//        driver.findElement(By.id("overlay")).click();
//        driver.switchTo().frame(driver.findElement(By.id("overlay")));

//        WebElement select = driver.findElement(By.id("useriLogInDropDown"));
//        select.sendKeys("San Diego PIS Area.Identifier2 [SanDiego_PIS.Area.Identifier@aphis.usda.gov]");
         String SD_names [] = { "San Diego PIS Area.Identifier2 [SanDiego_PIS.Area.Identifier@aphis.usda.gov]",
                                "San Diego PIS.Inspector [SanDiego_PIS.Inspector@aphis.usda.gov]",
                                "San Diego PIS.Supervisor [SanDiego_PIS.Supervisor@aphis.usda.gov]"};
         String NS_names [] = { "National.Specialist10 [National.Specialist10@aphis.usda.gov]",
                                "National.Specialist2 [National.Specialist2@aphis.usda.gov]",
                                "National.Specialist3 [National.Specialist3@aphis.usda.gov]"};
         String SF_names [] = { "San Fran Area.Identifier3 [SanFran_PIS.Area.Identifier3@aphis.usda.gov]",
                                "San Fran PPQ.Inspector3 [SanFran_PIS.PPQ.Inspector3@aphis.usda.gov]",
                                "San Fran PIS.Supervisor [SanFran_PIS.Supervisor@aphis.usda.gov]"};

//
//        National.Specialist1 [National.Specialist1@aphis.usda.gov]
//        National.Specialist2 [National.Specialist2@aphis.usda.gov]
//        National.Specialist3 [National.Specialist3@aphis.usda.gov]
//        A.Sysadmin [system.admin@aphis.usda.gov]
//        San Fran Area.Identifier3 [SanFran_PIS.Area.Identifier3@aphis.usda.gov]
//        San Fran PPQ.Inspector3 [SanFran_PIS.PPQ.Inspector3@aphis.usda.gov]
//        San Fran PIS.Supervisor [SanFran_PIS.Supervisor@aphis.usda.gov]

//        Actions action = new Actions(driver);
//        action.moveToElement(select).click().perform();
//        select.click();
//
//        JavascriptExecutor jse = (JavascriptExecutor) driver;
//        jse.executeScript("document.getElementById('usernameLogInDropDown').focus();");

//        WebElement Options = driver.findElement(By.id("usernameLogInDropDown"));
//        San Diego PIS Area.Identifier2 [SanDiego_PIS.Area.Identifier@aphis.usda.gov]

        Select dropDown = new Select(driver.findElement(By.xpath("//select[@id='usernameLogInDropDown']")));
//        dropDown.selectByVisibleText("San Diego PIS.Inspector [SanDiego_PIS.Inspector@aphis.usda.gov]");
        dropDown.selectByVisibleText(SF_names[1]);

//        List<WebElement> Options = driver.findElements(By.tagName("option"));
//        for(WebElement option: Options){
//            System.out.println(option.getText());
//            if (option.getText().equals("San Diego PIS.Inspector [SanDiego_PIS.Inspector@aphis.usda.gov]")) {
//                option.sendKeys("San Diego PIS.Inspector [SanDiego_PIS.Inspector@aphis.usda.gov]");
//                option.click();
//
//                break;
//            }
//        }
        driver.findElement(By.id("loginButton")).click();

        //Enter Shipment information screen
        driver.findElement(By.id("shipment_dashbaord_type")).sendKeys("Mail");
        driver.findElement(By.id("shipment_dashbaord_type")).sendKeys("Truck");
        driver.findElement(By.id("shipmentDashboard_new")).click();


        driver.findElement(By.id("portOfArrival")).sendKeys("Anchorage");
        driver.findElement(By.id("countryOfLading")).sendKeys("Aruba");

        //Arrived data picker
//      driver.findElement(By.cssSelector("img.ui-datepicker-trigger")).sendKeys("link=27");
        driver.findElement(By.cssSelector("img.ui-datepicker-trigger")).click();
//        String today = driver.findElement(By.cssSelector("a.ui-datepicker-days-cell-over.ui-state-highlight.ui-state-active]")).getText();
        String today = driver.findElement(By.className("ui-datepicker-today")).getText();
        driver.findElement(By.className("ui-datepicker-today")).click();
        System.out.print(today);
        System.out.println(" = today from datepicker" + "\narrive date is = ");
        today = driver.findElement(By.id("arrivedInUsDatepicker")).getAttribute("value");
        System.out.print(driver.findElement(By.id("arrivedInUsDatepicker")).getAttribute("value"));

        //PIS Date picker
//        driver.findElement(By.id("inStationDateTimeDatepicker")).sendKeys(today);
        driver.findElement(By.xpath("//input[@id='inStationDateTimeDatepicker']/following-sibling::img")).click();
        driver.findElement(By.className("ui-datepicker-today")).click();

//        driver.findElement(By.cssSelector("img.ui-datepicker-trigger")).click();
//        driver.findElement(By.className("ui-datepicker-today")).click();

        System.out.println("\n" + today);
        System.out.print(" = PIS date in datepicker" + "\nPIS date is = ");
        System.out.print(driver.findElement(By.id("inStationDateTimeDatepicker")).getAttribute("value"));
//        driver.findElement(By.id("inStationDateTimeDatepicker")).sendKeys("27");
//        driver.findElement(By.xpath("(//img[@alt='...'])[2]")).click();
        //Use System.currentTimeMillis() Master Air Waybill
        String masterAirWayBill = String.valueOf(System.currentTimeMillis());
        driver.findElement(By.id("shipmentTypeNumberModelForExisting0.number")).sendKeys(masterAirWayBill);
//        driver.findElement(By.id("shipmentTypeNumberModelForExisting0.number")).sendKeys("0");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting1.number")).sendKeys("1");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting2.number")).sendKeys("2");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting3.number")).sendKeys("3");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting4.number")).sendKeys("4");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting5.number")).sendKeys("5");
        driver.findElement(By.id("addShipmentType")).sendKeys("Master Air Waybill");
        driver.findElement(By.id("addShipmentNumber")).sendKeys("6");
        driver.findElement(By.id("shipperId")).sendKeys("Global Transport, Inc");
        driver.findElement(By.id("brokerId")).sendKeys("Test Broker 1");
        driver.findElement(By.id("consignerId")).sendKeys("CONTEL FRESH INC");

        driver.findElement(By.id("add")).click();
        cU.pause(5);
        driver.findElement(By.id("next")).click();
        cU.pause(5);
//        assertEquals("Enter Commodity Information",driver.getTitle());

        // Enter commodity Information Page
        driver.findElement(By.id("permitTypeId")).sendKeys("PPQ 621");
        String permitNumber = String.valueOf(System.currentTimeMillis());
        System.out.println("Current cU.currentDateAndTime() is" + cU.currentDateAndTime());

        driver.findElement(By.id("permitNumber")).sendKeys(permitNumber);
        driver.findElement(By.id("userCommodityId")).sendKeys("Cotton");
        driver.findElement(By.id("farmId")).sendKeys("Producer placeholder");
//        driver.findElement(By.id("taxonomicName")).sendKeys("Abelia");
        driver.findElement(By.id("propagativeMaterial")).sendKeys("Bulbs/Corms/Rhizomes");
        driver.findElement(By.id("countryOfOrgin")).sendKeys("Aruba");

        driver.findElement(By.id("quantity")).sendKeys("100");
        driver.findElement(By.id("units")).sendKeys("Square Meters");
        driver.findElement(By.id("woodPackingMaterial")).sendKeys("Compliant");
        driver.findElement(By.id("add")).click();
        driver.findElement(By.id("next")).click();
        cU.pause(15);

//      //Add pest on Enter Inspection Details
        driver.findElement(By.id("commodities0.commodityInspectionDate")).sendKeys(today);
        driver.findElement(By.id("commodities0.quantityInspected")).sendKeys("3");
        driver.findElement(By.id("commodities0.plantConditionId")).sendKeys("Good");
        driver.findElement(By.id("commodities0.remark")).sendKeys("Remark");
        driver.findElement(By.id("commodities0.pestPresentFlag")).sendKeys("Yes");

        driver.findElement(By.xpath("//input[@id='save']")).click();
        cU.pause(5);
        driver.findElement(By.xpath("//input[@id='pest']")).click();

//        driver.findElement(By.id("pest")).click();
        //
//        driver.findElement(By.id("RadioGroup1_2")).wait();
        cU.pause(5);
        driver.findElement(By.id("RadioGroup1_2")).isSelected();
        driver.findElement(By.id("RadioGroup1_2")).click();


        // Select other material
//        driver.findElement(By.id("RadioGroup1_3")).isSelected();
//        driver.findElement(By.id("railPastDiagnostic.select.material")).sendKeys("Wood Packing Material");

        //Select other material
        driver.findElement(By.id("railPastDiagnostic.select.material")).click();
        driver.findElement(By.id("diagnostic.radio.pest")).sendKeys("Wood Packing Material");

        //Select host
        driver.findElement(By.id("checkboxA0")).click();
        driver.findElement(By.id("railPastDiagnostic.checkbox.unsurecountry0")).click();
        driver.findElement(By.id("railPastDiagnostic.select.hostProximit0")).click();
        driver.findElement(By.id("railPastDiagnostic.select.hostProximit0")).sendKeys("On");
        driver.findElement(By.id("railPastDiagnostic.select.hostPart0")).sendKeys("Bagging");
        driver.findElement(By.id("railPastDiagnostic.select.cargoStatus0")).sendKeys("Held for this pest");
        driver.findElement(By.id("addPestCommodityModels0.releaseJustification")).sendKeys("release Justification");

        //3. Select a Pest/Pest Discipline
        driver.findElement(By.id("diagnostic.radio.pest")).click();
        cU.pause(5);
        // Select a Pest
//      driver.findElement(By.xpath("//input[@id='taxonomicName']")).sendKeys("Abronia");
        WebElement pest_field = driver.findElement(By.id("taxonomicName"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].value=arguments[1]", pest_field, "Abronia");

        driver.findElement(By.id("date.ppqDetermination.datedetermined")).sendKeys(today);

        //Select Discipline
        driver.findElement(By.id("diagnostic.radio.pestdiscipline")).click();
        // Select Method
//        driver.findElement(By.xpath("//select[@id='select.ppqDetermination.method']")).sendKeys("Culture");
        driver.findElement(By.id("select.ppqDetermination.pestDiscipline")).sendKeys("Entomology");
        driver.findElement(By.xpath("//select[@id='select.ppqDetermination.pestDiscipline']")).sendKeys("Nematology");
        driver.findElement(By.id("add")).click();

        //select discipline
        driver.findElement(By.id("text.ppqDetermination.aliveimmature")).click();
        driver.findElement(By.id("text.ppqDetermination.aliveimmature")).sendKeys(masterAirWayBill);
        driver.findElement(By.id("text.ppqDetermination.aliveimmature")).click();
        driver.findElement(By.id("text.ppqDetermination.deadimmature")).sendKeys("2");

        driver.findElement(By.id("addPestModels0.picked1")).click();

//        driver.findElement(By.id("submit")).click();
        driver.findElement(By.xpath("//input[@type='submit']")).click();

//        driver.findElement(By.id("next")).click();

//        selenium.select("id=portOfArrival", "label=Anchorage");
//        selenium.select("id=countryOfLading", "label=Aruba");
//        selenium.click("css=img.ui-datepicker-trigger");
//        selenium.click("link=27");
//        selenium.click("xpath=(//img[@alt='...'])[2]");
//        selenium.click("link=26");
//        selenium.type("id=shipmentTypeNumberModelForExisting0.number", "1");
//        selenium.type("id=shipmentTypeNumberModelForExisting1.number", "2");
//        selenium.type("id=shipmentTypeNumberModelForExisting2.number", "3");
//        selenium.type("id=shipmentTypeNumberModelForExisting3.number", "4");
//        selenium.type("id=shipmentTypeNumberModelForExisting4.number", "5");
//        selenium.type("id=shipmentTypeNumberModelForExisting5.number", "6");
//        selenium.select("id=addShipmentType", "label=Master Air Waybill");
//        selenium.type("id=addShipmentNumber", "7");
//        selenium.click("id=add");
//        selenium.waitForPageToLoad("30000");
//        selenium.click("id=next");
//        selenium.waitForPageToLoad("30000");
//        selenium.select("id=permitTypeId", "label=PPQ 621");
//        selenium.type("id=permitNumber", "1");
//        selenium.type("id=taxonomicName", "2");
//        selenium.select("id=propagativeMaterial", "label=Bulbs/Corms/Rhizomes");
//        selenium.select("id=countryOfOrgin", "label=Aruba");
//        selenium.type("id=quantity", "3");
//        selenium.select("id=units", "label=Square Meters");
//        selenium.select("id=woodPackingMaterial", "label=Compliant");
//        selenium.click("id=add");
//        selenium.waitForPageToLoad("30000");

        cU.pause(50);
    }

}
