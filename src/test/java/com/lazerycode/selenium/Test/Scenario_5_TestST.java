package com.lazerycode.selenium.Test;

import com.lazerycode.selenium.CommonUtil;
import com.lazerycode.selenium.Pages.*;
import com.lazerycode.selenium.ScreenshotListener;
import com.lazerycode.selenium.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ResourceBundle;

import static junit.framework.Assert.assertEquals;

//import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 3/25/14
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */

@Listeners(ScreenshotListener.class)
public class Scenario_5_TestST extends SeleniumBase {

    public static ResourceBundle _prop = ResourceBundle.getBundle("demo");

    @Test
    public void HappyPath() throws Exception {
        CommonUtil cU = new CommonUtil();
        String url_string = _prop.getString("url_string");
        System.out.println("Accessing URL ==> " + url_string);
        if (url_string == null){
            System.err.println("Unknown browser specified, defaulting to Dev URL http://10.101.25.23:8080/armWeb2/ ");
            url_string ="http://10.101.25.23:8080/armWeb2/";
        }

        WebDriver driver = getDriver();
        driver.get(url_string);
        assertEquals("ARM - Arm Homepage", driver.getTitle());

        //Logon page
        LogonPage a = new LogonPage(driver);
        String user_inspector = _prop.getString("user_inspector");
        String password = _prop.getString("password");
        System.out.println(user_inspector + "<====username, password ==>" + password);
        a.armLogin(driver, user_inspector, password);
        cU.pause(5);

        //Shipment page
        //new ShipmentPage(driver).armShipment();
        ShipmentPage b = new ShipmentPage(driver);
        b.armShipment(driver, "Air");

        //Commodity page
        CommodityPage c = new CommodityPage(driver);
        c.armCommodity(driver);

        //Inspection page
        InspectionPage d = new InspectionPage(driver);
        d.armInspection(driver);

        //Pest page
        PestPage e = new PestPage(driver);
        e.armPest(driver);

        //e.armLogout();
        String DRNum = e.armGetDRNum();
        CommonPageElement cpe = new CommonPageElement(driver);
        cpe.armLogout(driver);

        //Area identifier
        String user_areaidentifier = _prop.getString("user_areaidentifier");
        a.armLogin(driver, user_areaidentifier, password);

        //AI Dashboard
        DashBoardPage dashboardAI = new DashBoardPage(driver);
        dashboardAI.select_AI_DR_NumLink(driver, DRNum);

        //AI Enter Determination
        EnterDeterminationPage ed = new EnterDeterminationPage(driver);
        ed.armEnterDetermination(driver);

        //AI Quarantine Recommendation
        QuarantineRecommendationPage qr = new QuarantineRecommendationPage(driver);
        qr.armNoPhytoAction(driver);

        //Back to Inspector Issue EAN
        cpe.armLogout(driver);
        a.armLogin(driver, user_inspector, password);

        DashBoardPage dashboardInspector = new DashBoardPage(driver);
        //b.getLRN() will return the LRN # entered before
//        dashboardInspector.select_Inspector_EAN_ID_Link(driver, b.getLRN());
        dashboardInspector.select_Inspector_EAN_ID_Link(driver, "LRN1399490612851");
        //Inspector EAN Information

    }
}
