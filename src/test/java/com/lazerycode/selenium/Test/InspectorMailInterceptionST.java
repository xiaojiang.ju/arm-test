package com.lazerycode.selenium.Test;

import com.lazerycode.selenium.ScreenshotListener;
import com.lazerycode.selenium.SeleniumBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.lazerycode.selenium.CommonUtil;
import static junit.framework.Assert.assertEquals;

//import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: alexju
 * Date: 3/25/14
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */

@Listeners(ScreenshotListener.class)
public class InspectorMailInterceptionST extends SeleniumBase {

    // customized wait function
//    public static boolean wait_pageLoad(long seconds){
//        WebDriverWait wait = new WebDriverWait.FIVE_HUNDRED_MILLIS();
//        WebDriver driver = getDriver();
//        return wait.until(driver.findElement(By.xpath("//*[not (.='')]")));
//    }

    @Test
    public void testLoginAI() throws Exception {
        CommonUtil cU = new CommonUtil();
        WebDriver driver = getDriver();
        //QA environment
        driver.get("http://10.101.25.23:8080/armWeb2/");

        driver.findElement(By.id("loginBtn")).click();
        assertEquals("Arm Homepage",driver.getTitle());

         String SD_names [] = { "San Diego PIS Area.Identifier2 [SanDiego_PIS.Area.Identifier@aphis.usda.gov]",
                                "San Diego PIS.Inspector [SanDiego_PIS.Inspector@aphis.usda.gov]",
                                "San Diego PIS.Supervisor [SanDiego_PIS.Supervisor@aphis.usda.gov]"};
         String NS_names [] = { "National.Specialist10 [National.Specialist10@aphis.usda.gov]",
                                "National.Specialist2 [National.Specialist2@aphis.usda.gov]",
                                "National.Specialist3 [National.Specialist3@aphis.usda.gov]"};
         String SF_names [] = { "San Fran Area.Identifier3 [SanFran_PIS.Area.Identifier3@aphis.usda.gov]",
                                "San Fran PPQ.Inspector3 [SanFran_PIS.PPQ.Inspector3@aphis.usda.gov]",
                                "San Fran PIS.Supervisor [SanFran_PIS.Supervisor@aphis.usda.gov]"};

        Select dropDown = new Select(driver.findElement(By.xpath("//select[@id='usernameLogInDropDown']")));
//        dropDown.selectByVisibleText("San Diego PIS.Inspector [SanDiego_PIS.Inspector@aphis.usda.gov]");
        dropDown.selectByVisibleText(SD_names[1]);

        driver.findElement(By.id("loginButton")).click();

        //Enter Shipment information screen
        driver.findElement(By.id("shipment_dashbaord_type")).sendKeys("Mail");
        driver.findElement(By.id("shipmentDashboard_new")).click();
//        assertEquals("Arm Homepage",driver.getTitle());


        driver.findElement(By.id("portOfArrival")).sendKeys("Anchorage");
        driver.findElement(By.id("countryOfLading")).sendKeys("Aruba");

        //Arrived data picker
        driver.findElement(By.cssSelector("img.ui-datepicker-trigger")).click();
//        String today = driver.findElement(By.cssSelector("a.ui-datepicker-days-cell-over.ui-state-highlight.ui-state-active]")).getText();
        String today = driver.findElement(By.className("ui-datepicker-today")).getText();
        driver.findElement(By.className("ui-datepicker-today")).click();
        System.out.print(today);
        System.out.println(" = today from datepicker" + "\narrive date is = ");
        today = driver.findElement(By.id("arrivedInUsDatepicker")).getAttribute("value");;
        System.out.print(driver.findElement(By.id("arrivedInUsDatepicker")).getAttribute("value"));

        //PIS Date picker
        driver.findElement(By.xpath("//input[@id='inStationDateTimeDatepicker']/following-sibling::img")).click();
        driver.findElement(By.className("ui-datepicker-today")).click();

        driver.findElement(By.id("shipmentTypeNumberModelForExisting0.number")).sendKeys("0");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting1.number")).sendKeys("1");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting2.number")).sendKeys("2");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting3.number")).sendKeys("3");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting4.number")).sendKeys("4");
        driver.findElement(By.id("shipmentTypeNumberModelForExisting5.number")).sendKeys("5");
        driver.findElement(By.id("addShipmentType")).sendKeys("Master Air Waybill");
        driver.findElement(By.id("addShipmentNumber")).sendKeys("6");
        cU.pause(5);
        driver.findElement(By.id("add")).click();
        cU.pause(5);
        driver.findElement(By.id("next")).click();
        cU.pause(5);
//        assertEquals("Enter Commodity Information",driver.getTitle());

        // Enter commodity Information Page
        driver.findElement(By.id("permitTypeId")).sendKeys("PPQ 621");
        driver.findElement(By.id("permitNumber")).sendKeys("1");
        driver.findElement(By.id("userCommodityId")).sendKeys("Cotton");
        driver.findElement(By.id("producer")).sendKeys("farm placeholder");
//        driver.findElement(By.id("taxonomicName")).sendKeys("Abelia");
        driver.findElement(By.id("propagativeMaterial")).sendKeys("Bulbs/Corms/Rhizomes");
        driver.findElement(By.id("countryOfOrgin")).sendKeys("Aruba");
        driver.findElement(By.id("quantity")).sendKeys("3");
        driver.findElement(By.id("units")).sendKeys("Square Meters");
        driver.findElement(By.id("woodPackingMaterial")).sendKeys("Compliant");
        driver.findElement(By.id("add")).click();
        cU.pause(5);
        driver.findElement(By.id("next")).click();

//      //Add pest on Enter Inspection Details
         driver.findElement(By.id("commodities0.commodityInspectionDate")).sendKeys(today);
        driver.findElement(By.id("commodities0.quantityInspected")).sendKeys("3");
        driver.findElement(By.id("commodities0.plantConditionId")).sendKeys("Good");
        driver.findElement(By.id("commodities0.remark")).sendKeys("Remark");
        driver.findElement(By.id("commodities0.remark")).sendKeys("Remark");
        driver.findElement(By.id("commodities0.pestPresentFlag")).sendKeys("Yes");

        driver.findElement(By.id("pest")).click();
        cU.pause(5);
        driver.findElement(By.id("RadioGroup1_3")).click();
        driver.findElement(By.id("railPastDiagnostic.select.material")).click();
        driver.findElement(By.id("diagnostic.radio.pest")).sendKeys("Wood Packing Material");

        driver.findElement(By.id("date.ppqDetermination.datedetermined")).sendKeys(today);
        driver.findElement(By.id("diagnostic.radio.pestdiscipline")).click();
        driver.findElement(By.id("select.ppqDetermination.pestDiscipline")).sendKeys("Entomology");

        driver.findElement(By.id("text.ppqDetermination.aliveimmature")).sendKeys("alive immature");
        driver.findElement(By.id("text.ppqDetermination.deadimmature")).sendKeys("dead immature");
        driver.findElement(By.id("add")).click();
        driver.findElement(By.id("submit")).click();
////


//        driver.findElement(By.id("next")).click();

//        selenium.select("id=portOfArrival", "label=Anchorage");
//        selenium.select("id=countryOfLading", "label=Aruba");
//        selenium.click("css=img.ui-datepicker-trigger");
//        selenium.click("link=27");
//        selenium.click("xpath=(//img[@alt='...'])[2]");
//        selenium.click("link=26");
//        selenium.type("id=shipmentTypeNumberModelForExisting0.number", "1");
//        selenium.type("id=shipmentTypeNumberModelForExisting1.number", "2");
//        selenium.type("id=shipmentTypeNumberModelForExisting2.number", "3");
//        selenium.type("id=shipmentTypeNumberModelForExisting3.number", "4");
//        selenium.type("id=shipmentTypeNumberModelForExisting4.number", "5");
//        selenium.type("id=shipmentTypeNumberModelForExisting5.number", "6");
//        selenium.select("id=addShipmentType", "label=Master Air Waybill");
//        selenium.type("id=addShipmentNumber", "7");
//        selenium.click("id=add");
//        selenium.waitForPageToLoad("30000");
//        selenium.click("id=next");
//        selenium.waitForPageToLoad("30000");
//        selenium.select("id=permitTypeId", "label=PPQ 621");
//        selenium.type("id=permitNumber", "1");
//        selenium.type("id=taxonomicName", "2");
//        selenium.select("id=propagativeMaterial", "label=Bulbs/Corms/Rhizomes");
//        selenium.select("id=countryOfOrgin", "label=Aruba");
//        selenium.type("id=quantity", "3");
//        selenium.select("id=units", "label=Square Meters");
//        selenium.select("id=woodPackingMaterial", "label=Compliant");
//        selenium.click("id=add");
//        selenium.waitForPageToLoad("30000");

        cU.pause(50);
    }

}
